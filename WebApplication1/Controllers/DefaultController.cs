﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Helper;

namespace WebApplication1.Controllers
{
    [RoutePrefix("")]
    public class DefaultController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("metataggenerator")]
        public ActionResult MetaTagGenerator()
        {
            return View();
        }

        [Route("thumbgenerator")]
        public ActionResult ThumbGenerator()
        {
            return View();
        }

        [Route("textcounter")]
        public ActionResult TextCounter()
        {
            return View();
        }

        [Route("developerchecker")]
        public ActionResult DeveloperChecker()
        {
            return View();
        }

        [Route("textgenerator")]
        public ActionResult TextGenerator()
        {
            return View();
        }

        [Route("imagegenerator")]
        public ActionResult ImageGenerator(int width = 200, int height = 200, string text = "200x200", int fontsize = 20, string textColor = "black", string backColor = "white")
        {
            Image img = new Bitmap(width, height);
            var drawing = Graphics.FromImage(img);

            var objFont = new Font(FontFamily.GenericSansSerif, fontsize, FontStyle.Regular, GraphicsUnit.Pixel);

            img.Dispose();
            drawing.Dispose();

            img = new Bitmap(width, height);

            drawing = Graphics.FromImage(img);

            var bcolor = Color.FromName(backColor);
            drawing.Clear(bcolor);

            var sf = new StringFormat
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };

            var tcolor = Color.FromName(textColor);

            Brush textBrush = new SolidBrush(tcolor);
            drawing.TextRenderingHint = TextRenderingHint.AntiAlias;
            drawing.DrawString(text, objFont, textBrush, width / 2, height / 2, sf);

            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            var bmp = (Bitmap)img;
            var bitmapBytes = BitmapToBytes(bmp);
            return File(bitmapBytes, "image/jpeg");
        }

        private static byte[] BitmapToBytes(Bitmap img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, ImageFormat.Png);
                return stream.ToArray();
            }
        }

        [Route("htmldecodeencode")]
        public ActionResult HtmlDecodeEncode()
        {
            return View();
        }

        [HttpPost]
        [Route("htmldecoder")]
        public JsonResult HtmlDecoder(string txt)
        {
            var decodedText = HttpUtility.HtmlDecode(txt);
            return Json(decodedText);
        }

        [HttpPost]
        [Route("htmlencoder")]
        public JsonResult HtmlEncoder(string txt)
        {
            var encodedText = HttpUtility.HtmlEncode(txt);
            return Json(encodedText);
        }

        [Route("urldecodeencode")]
        public ActionResult UrlDecodeEncode()
        {
            return View();
        }

        [HttpPost]
        [Route("urldecoder")]
        public JsonResult UrlDecoder(string txt)
        {
            var decodedText = HttpUtility.UrlDecode(txt);
            return Json(decodedText);
        }

        [HttpPost]
        [Route("urlencoder")]
        public JsonResult UrlEncoder(string txt)
        {
            var encodedText = HttpUtility.UrlEncode(txt);
            return Json(encodedText);
        }

        [Route("codes")]
        public ActionResult Codes()
        {
            return View();
        }

        [Route("password")]
        public ActionResult Password()
        {
            return View();
        }

        [HttpPost]
        [Route("password")]
        public JsonResult Password(string l = "12", string s = "", string n = "", string t = "")
        {
            //şifre parametreleri
            const string kucukData = "abcdefghijklmnopqrstuvwxyz";
            const string buyukData = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string ozelData = "!$&()[{}=_:;|$€";
            const string sayiData = "0123456789";
            var finalData = "";
            //başlangıç şifresi oluştur
            finalData = kucukData + buyukData + kucukData + buyukData;
            //oluşacak şifre
            var password = "";

            var _l = Convert.ToInt32(l);
            var _s = (!string.IsNullOrEmpty(s));
            var _n = (!string.IsNullOrEmpty(n));

            //özel karakter ve sayı eklenirse
            if (_s) finalData += ozelData; finalData += ozelData; finalData += ozelData;
            if (_n) finalData += sayiData; finalData += sayiData; finalData += sayiData;

            //şifreyi karıştır
            finalData = ShuffleHelper.StringMixer(finalData);
            finalData = ShuffleHelper.StringMixer(finalData);
            finalData = ShuffleHelper.StringMixer(finalData);

            //şifreyi oluştur
            var res = new StringBuilder();
            var rnd = new Random();
            while (0 < _l--) res.Append(finalData[rnd.Next(finalData.Length)]);

            //şifre başında kelime istenirse
            password = (string.IsNullOrEmpty(t) ? res.ToString() : t.ToString() + res.ToString());

            return Json(password);
        }

        [Route("cssminifier")]
        public ActionResult CssMinifier()
        {
            return View();
        }

        [HttpPost]
        [Route("cssminifier")]
        public JsonResult CssMinifier(string txt)
        {
            var cssMinified = CssMinifierHelper.Minify(txt);
            return Json(cssMinified);
        }

        [Route("javascriptminifier")]
        public ActionResult JavascriptMinifier()
        {
            return View();
        }

        [HttpPost]
        [Route("javascriptminifier")]
        public JsonResult JavascriptMinifier(string txt)
        {
            var cssMinified = JavascriptMinifierHelper.Minify(txt);
            return Json(cssMinified);
        }

        [Route("htmlminifier")]
        public ActionResult HtmlMinifier()
        {
            return View();
        }

        [HttpPost]
        [Route("htmlminifier")]
        public JsonResult HtmlMinifier(string txt)
        {
            var cssMinified = HtmlMinifierHelper.Minify(txt);
            return Json(cssMinified);
        }

        [Route("imageminifier")]
        public ActionResult ImageMinifier()
        {
            ViewBag.Result = TempData["result"];
            return View();
        }

        [HttpPost]
        [Route("imageminifier")]
        public ActionResult ImageMinifier(HttpPostedFileBase img)
        {
            var result = ImageMinifierHelper.Minify(img);
            TempData["result"] = result;
            return RedirectToActionPermanent("ImageMinifier");
        }

        [Route("htmltotext")]
        public ActionResult HtmlToText()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [Route("htmltotext")]
        public JsonResult HtmlToText(string txt)
        {
            var result = Regex.Replace(txt, "<.*?>", string.Empty);
            return Json(result);
        }

        [Route("ftp")]
        public ActionResult FtpUploader()
        {
            ViewBag.Result = TempData["result"];
            return View();
        }

        [HttpPost]
        [Route("ftp")]
        [ValidateAntiForgeryToken]
        public ActionResult FtpUploader(HttpPostedFileBase img)
        {
            try
            {
                var text = Guid.NewGuid() + img.FileName;
                var str1 = Server.MapPath("~/Assets/Content/Temp/");
                img.SaveAs(str1 + text);
                string ImagePath = str1 + text;

                //Create FTP request
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://ftp.smarterasp.net/tools/assets/general/" + img.FileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential("kayakiranh-001", "maldaracanu1988");
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Load the file
                FileStream stream = System.IO.File.OpenRead(ImagePath);
                byte[] buffer = new byte[stream.Length];

                stream.Read(buffer, 0, buffer.Length);
                stream.Close();

                //Upload file
                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Close();

                TempData["result"] = "ok";
                return RedirectToActionPermanent("FtpUploader");
            }
            catch (Exception ex)
            {
                TempData["result"] = "error : " + ex.InnerException.Message;
                return RedirectToActionPermanent("FtpUploader");
            }
        }

        [Route("ocr")]
        public ActionResult Ocr()
        {
            ViewBag.Result = TempData["result"];
            return View();
        }

        [HttpPost]
        [Route("ocr")]
        [ValidateAntiForgeryToken]
        public ActionResult Ocr(HttpPostedFileBase img)
        {
            var result = OcrHelper.Read(img);
            TempData["result"] = result.Result;
            return RedirectToActionPermanent("Ocr");
        }

        [Route("ping")]
        public ActionResult Ping()
        {
            return View();
        }

        [HttpPost]
        [Route("ping")]
        public JsonResult Ping(string txt)
        {
            try
            {
                txt = txt.Replace("www.", "");
                txt = txt.Replace("http://", "");
                txt = txt.Replace("https://", "");
            }
            catch { }

            Ping _ping = new Ping();
            string result = "";
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    PingReply _cevap = _ping.Send(txt);

                    result += "Deneme " + i.ToString() + " : <br>";

                    if (_cevap.Status == IPStatus.Success)
                    {
                        result += "IP : " + _cevap.Address.ToString() + " Süre : " + _cevap.RoundtripTime.ToString() + " Aktif : " + _cevap.Options.Ttl.ToString() + "<br>";
                    }
                    else if (_cevap.Status == IPStatus.TimedOut)
                    {
                        result += "Sorgu zaman aşımına uğradı. <br>";
                    }
                    else
                    {
                        result += "Bilinmeyen Hata. <br>";
                    }
                }
                catch (Exception ex)
                {
                    result += "Hata : " + ex.InnerException.Message + "<br>";
                }
            }

            return Json(result);
        }

        [Route("jsonbeautifier")]
        public ActionResult JsonBeautifier()
        {
            return View();
        }

        [HttpPost]
        [Route("jsonbeautifier")]
        public JsonResult JsonBeautifier(string txt)
        {
            dynamic parsedJson = JsonConvert.DeserializeObject(txt);
            var json = JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
            return Json(json);
        }

        [Route("jsontoclass")]
        public ActionResult JsonToClass()
        {
            return View();
        }

        [HttpPost]
        [Route("jsontoclass")]
        public JsonResult JsonToClass(string txt)
        {
            var json = Server.HtmlEncode(JsonToClassConverter.Prepare(txt, "Example", 1, true, true, "None", false));
            return Json(json);
        }

        [HttpPost]
        [Route("jsontosql")]
        public JsonResult JsonToSQL(string txt)
        {
            var json = Server.HtmlEncode(JsonToClassConverter.Prepare(txt, "Example", 4, true, true, "None", false));
            return Json(json);
        }
    }
}