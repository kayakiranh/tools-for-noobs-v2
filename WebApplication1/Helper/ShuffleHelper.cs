﻿using System;

namespace WebApplication1.Helper
{
    public class ShuffleHelper
    {
        private static readonly Random Rnd = new Random();

        private static void Fisher_Yates(int[] array)
        {
            var arraysize = array.Length;

            for (var i = 0; i < arraysize; i++)
            {
                var random = i + (int)(Rnd.NextDouble() * (arraysize - i));

                var temp = array[random];
                array[random] = array[i];
                array[i] = temp;
            }
        }

        public static string StringMixer(string s)
        {
            var output = "";
            var arraysize = s.Length;
            var randomArray = new int[arraysize];

            for (var i = 0; i < arraysize; i++) randomArray[i] = i;

            Fisher_Yates(randomArray);

            for (var i = 0; i < arraysize; i++) output += s[randomArray[i]];

            return output;
        }
    }
}