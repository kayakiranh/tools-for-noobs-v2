﻿using Newtonsoft.Json.Linq;
using System.ComponentModel;
using System.IO;
using Xamasoft.JsonClassGenerator;
using Xamasoft.JsonClassGenerator.CodeWriters;

namespace WebApplication1.Helper
{
    public static class JsonToClassConverter
    {
        private static readonly ICodeWriter[] CodeWriters = new ICodeWriter[] {
            new CSharpCodeWriter(),
            new VisualBasicCodeWriter(),
            new TypeScriptCodeWriter()
        };

        public static string Prepare(string JSON, string classname, int language, bool nest, bool pascal, string propertyAttribute, bool hasGetSet = false)
        {
            if (string.IsNullOrEmpty(JSON))
            {
                return null;
            }

            ICodeWriter writer;

            switch (language)
            {
                case 1:
                    writer = new CSharpCodeWriter();
                    break;
                case 2:
                    writer = new VisualBasicCodeWriter();
                    break;
                case 7:
                    writer = new TypeScriptCodeWriter();
                    break;
                case 4:
                    writer = new SqlCodeWriter();
                    break;
                case 5:
                    writer = new JavaCodeWriter();
                    break;
                default:
                    writer = new PhpCodeWriter();
                    break;
            }

            var gen = new JsonClassGenerator
            {
                Example = JSON,
                InternalVisibility = false,
                CodeWriter = writer,
                ExplicitDeserialization = false,
                Namespace = nest ? "JSONUtils" : null,
                NoHelperClass = false,
                SecondaryNamespace = null,
                UseProperties = (language != 5 && language != 6) || hasGetSet,
                MainClass = classname,
                UsePascalCase = pascal,
                PropertyAttribute = propertyAttribute,
                UseNestedClasses = nest,
                ApplyObfuscationAttributes = false,
                SingleFile = true,
                ExamplesInDocumentation = false,
                TargetFolder = null
            };

            gen.SingleFile = true;

            using (var sw = new StringWriter())
            {
                gen.OutputStream = sw;
                gen.GenerateClasses();
                sw.Flush();

                return sw.ToString();
            }
        }

        public static string Result { get; set; }

        public static void R(object d)
        {
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(d))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(d);
                Result += string.Format("{0}={1}", name, value);

                if (value is JToken && ((JToken)value).Type == JTokenType.Object)
                    R(value);
            }
        }
    }
}