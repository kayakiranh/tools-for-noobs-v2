﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication1.Helper
{
    public class OcrHelper
    {
        public static async Task<string> Read(HttpPostedFileBase file)
        {
            //dosyayı yükle
            var text = Guid.NewGuid() + file.FileName;
            var str1 = HttpContext.Current.Server.MapPath("~/Assets/Content/Temp/");
            file.SaveAs(str1 + text);
            string ImagePath = str1 + text;
            //linkini ver
            var link = "http://tools.huseyinkayakiran.com/Assets/Content/Temp/" + text;

            var finalData = "";

            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://api.ocr.space/Parse/Image");
                request.Headers.Add("apikey", "7134a12dd288957");

                var postData = "isOverlayRequired=true";
                postData += "&url=" + link;
                postData += "&language=tur";
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string[] zz = responseString.Split(new string[] { "WordText" }, StringSplitOptions.None);
                for (int i = 1; i < zz.Length; i++)
                {
                    try
                    {
                        finalData += zz[i].Substring(3, zz[i].IndexOf("\",\"Left\"") - 3) + " ";
                    }
                    catch
                    {
                        finalData += zz[i].Substring(3, zz[i].IndexOf("\",\"Left\"")) + " ";
                    }
                }
            }
            catch
            {
                var request = (HttpWebRequest)WebRequest.Create("https://api.ocr.space/Parse/Image");
                request.Headers.Add("apikey", "7134a12dd288957");

                var postData = "isOverlayRequired=true";
                postData += "&url=" + link;
                postData += "&language=eng,";
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string[] zz = responseString.Split(new string[] { "WordText" }, StringSplitOptions.None);
                for (int i = 1; i < zz.Length; i++)
                {
                    try
                    {
                        finalData += zz[i].Substring(3, zz[i].IndexOf("\",\"Left\"") - 3) + " ";
                    }
                    catch
                    {
                        finalData += zz[i].Substring(3, zz[i].IndexOf("\",\"Left\"")) + " ";
                    }
                }
            }

            
            return finalData;
        }
    }
}