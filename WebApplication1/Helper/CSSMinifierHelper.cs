﻿using System.IO;
using System.Net;
using System.Text;

namespace WebApplication1.Helper
{
    public class CssMinifierHelper
    {
        public static string Minify(string longCss)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://cssminifier.com/raw");
            request.Method = "POST";
            var formContent = "input=" + longCss;
            var byteArray = Encoding.UTF8.GetBytes(formContent);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            var str = request.GetRequestStream();
            str.Write(byteArray, 0, byteArray.Length);
            str.Close();

            var response = request.GetResponse();
            str = response.GetResponseStream();
            if (str != null)
            {
                var reader = new StreamReader(str);
                longCss = reader.ReadToEnd();
                reader.Close();
                str.Close();
            }

            response.Close();

            return longCss;
        }
    }
}