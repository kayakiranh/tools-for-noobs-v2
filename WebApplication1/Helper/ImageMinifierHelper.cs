﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace WebApplication1.Helper
{
    public class ImageMinifierHelper
    {
        public static ImageMinifierResult Minify(HttpPostedFileBase img)
        {
            var imr = new ImageMinifierResult();

            if (img != null &&
                (img.FileName.EndsWith(".jpg") || img.FileName.EndsWith(".jpeg") || img.FileName.EndsWith(".png")) &&
                img.ContentLength <= 5242880)
            {
                var num = 0L;

                var compressPercent = "";
                var compressTotal = "";
                var downloadLink = "";

                var text = Guid.NewGuid() + img.FileName;
                var str = HttpContext.Current.Server.MapPath("~/Assets/Content/Temp/");
                img.SaveAs(str + text);
                var str2 = "5x1V-PTQjauW6rs5LWxHk2ioIP89ux3j";
                var fileName = img.FileName;
                var address = "https://api.tinify.com/shrink";
                var webClient = new WebClient();
                var str3 = Convert.ToBase64String(Encoding.UTF8.GetBytes("api:" + str2));
                webClient.Headers.Add(HttpRequestHeader.Authorization, "Basic " + str3);
                var str4 = "";
                try
                {
                    var finalFile = new FileInfo(str + text);
                    for (var i = 1; i <= 3; i++)
                    {
                        if (i == 1)
                        {
                            webClient.UploadData(address, File.ReadAllBytes(str + text));
                            str4 = i + "_" + text;
                            var address2 = webClient.ResponseHeaders["Location"];
                            webClient.DownloadFile(address2, str + str4);
                            var fileInfo = new FileInfo(str + text);
                            num = fileInfo.Length;
                        }

                        if (i == 2)
                        {
                            webClient.UploadData(address, File.ReadAllBytes(str + str4));
                            str4 = i + "_" + text;
                            var address3 = webClient.ResponseHeaders["Location"];
                            webClient.DownloadFile(address3, str + str4);
                        }

                        if (i == 3)
                        {
                            webClient.UploadData(address, File.ReadAllBytes(str + str4));
                            var address4 = webClient.ResponseHeaders["Location"];
                            webClient.DownloadFile(address4, str + fileName);
                            finalFile = new FileInfo(str + fileName);
                        }
                    }

                    compressPercent = (100.0 - 100.0 * (Convert.ToDouble(finalFile.Length) / Convert.ToDouble(num))).ToString(CultureInfo.InvariantCulture);
                    compressTotal = finalFile.Length.ToString();
                    downloadLink = "/Assets/Content/Temp/2_" + text;
                }
                catch (Exception ex)
                {
                    downloadLink = ex.Message;
                }

                imr.CompressPercent = compressPercent;
                imr.CompressTotal = compressTotal;
                imr.DownloadLink = downloadLink;
            }

            return imr;
        }

        public class ImageMinifierResult
        {
            public string CompressPercent { get; set; }
            public string CompressTotal { get; set; }
            public string DownloadLink { get; set; }
        }
    }
}